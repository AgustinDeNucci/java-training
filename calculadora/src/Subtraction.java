package calculator;
public class Subtraction extends Operation{

    double subtraction;

    public Subtraction(double n1, double n2) {

        super(n1, n2,'-');
        this.subtraction = n1 - n2;
        this.setRes(this.subtraction);
    }
}
