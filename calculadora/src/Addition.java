package calculator;
public class Addition extends Operation{

    double sum;

    public Addition(double n1, double n2) {

        super(n1, n2, '+');
        this.sum = n1 + n2;
        this.setRes(this.sum);
    }
}