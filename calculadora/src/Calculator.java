package calculator;
import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) {

        double n1, n2;

        Scanner reader = new Scanner( System.in );
        //Mediante la clase System.in instanciamos el Scanner

        System.out.print( "Enter first number: " );
        n1 = reader.nextInt();
        //Para leer cada uno de los numeros nos apoyamos el método .nextInt().

        System.out.print( "Enter second number: " );
        n2 = reader.nextInt();

        Scanner sn = new Scanner(System.in);
        boolean exit = false;
        int opcion; //Guardamos la opcion del usuario

        while(!exit){

            System.out.println("1. Addition");
            System.out.println("2. Subtraction");
            System.out.println("3. Multiplication 3");
            System.out.println("4. Division");
            System.out.println("5. Exit");

            System.out.println("Choose one of the options");
            opcion = sn.nextInt();

            switch(opcion){
                case 1:
                    Addition sum = new Addition(n1,n2);
                    sum.ShowResult();
                    break;
                case 2:
                    Subtraction res = new Subtraction(n1,n2);
                    res.ShowResult();
                    break;
                case 3:
                    Multiplication mul = new Multiplication(n1,n2);
                    mul.ShowResult();
                    break;
                case 4:
                    Division div = new Division(n1,n2);
                    div.ShowResult();
                    break;
                case 5:
                    exit=true;
                    break;
                default:
                    System.out.println("Wrong Number");
            }

        }

    }
}