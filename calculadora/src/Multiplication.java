package calculator;
public class Multiplication extends Operation{

    double mult;

    public Multiplication(double n1, double n2) {

        super(n1, n2, '*');
        this.mult = n1 * n2;
        this.setRes(this.mult);
    }
}